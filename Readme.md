Mage
====

A CLI wizard tool similar to make but intended for intaractive usage.

## Example

```bash
# build project
cargo build

# run with example file
# (no actual commands are executed yet)
cargo run study -f examples/ok/dependency.yml
```

## Usage

Create a Magefile in YAML Format.

The magefile contains a list of tasks that can have a number of scripts
that complete them. Optionally a task can have a description and depend on other tasks.
For some examples, look at the examples folder in the source code.
By default, `mage` will look for `Magefile.yml` in the current directory.
