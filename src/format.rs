use fehler::{throw, throws};
use petgraph::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashSet};
use std::ops::Deref;

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct Scripts(Vec<String>);

#[derive(Debug, Serialize, Deserialize, PartialEq)]
enum Implementation {
    Any { scripts: Scripts },
    All { scripts: Scripts },
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(untagged)]
pub enum Script {
    Single(String),
    Multiple(Vec<String>),
}

impl Default for Script {
    fn default() -> Self {
        Self::Multiple(vec![])
    }
}

impl Script {
    #[allow(dead_code)]
    fn new(s: String) -> Self {
        Self::Single(s)
    }
}

impl Deref for Script {
    type Target = [String];
    fn deref(&self) -> &[String] {
        use Script::*;
        match self {
            Single(s) => std::slice::from_ref(s),
            Multiple(ss) => ss.deref(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Task {
    #[serde(default, skip_serializing_if = "is_default")]
    pub description: Option<String>,
    pub scripts: Script,
    #[serde(default, skip_serializing_if = "is_default")]
    pub depends_on: Vec<String>,
}

//this function is used to not serialize default values
fn is_default<T: Default + PartialEq>(t: &T) -> bool {
    t == &T::default()
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Data {
    #[serde(default, skip_serializing_if = "is_default")]
    pub version: Option<i32>,
    #[serde(flatten)]
    pub tasks: BTreeMap<String, Task>,
}

impl Data {
    #[throws(anyhow::Error)]
    pub fn from_reader<R>(reader: R) -> Self
    where
        R: std::io::Read,
    {
        serde_yaml::from_reader(reader)?
    }
}

#[derive(Debug)]
struct MissingErr {
    missing: HashSet<String>,
    required_by: HashSet<String>,
}

impl std::fmt::Display for MissingErr {
    #[throws(std::fmt::Error)]
    fn fmt(&self, fmt: &mut std::fmt::Formatter) {
        fmt.debug_struct("Missing Dependencies:")
            .field("missing", &self.missing)
            .field("required_by", &self.required_by)
            .finish()?;
    }
}

impl std::error::Error for MissingErr {}

#[throws(anyhow::Error)]
pub fn build_graph(d: &Data) -> DiGraph<String, ()> {
    let graph = build_graph_unchecked(d)?;
    anyhow::ensure!(
        !petgraph::algo::is_cyclic_directed(&graph),
        "There can't be cyclic dependencies"
    );

    graph
}

#[throws(anyhow::Error)]
pub fn build_graph_unchecked(d: &Data) -> DiGraph<String, ()> {
    let mut deps = Graph::<String, ()>::new();
    let mut nidx = BTreeMap::new();
    for key in d.tasks.keys() {
        nidx.insert(key.clone(), deps.add_node(key.clone()));
    }
    let mut missing = HashSet::new();
    let mut required_by = HashSet::new();
    for (a, value) in d.tasks.iter() {
        for b in value.depends_on.iter() {
            let first = *nidx.get(a).unwrap();
            let second = match nidx.get(b) {
                Some(&value) => value,
                None => {
                    required_by.insert(a.clone());
                    missing.insert(b.clone());
                    continue;
                }
            };
            deps.update_edge(first, second, ());
        }
    }
    if !missing.is_empty() {
        throw!(MissingErr {
            missing,
            required_by
        });
    }
    deps
}

#[cfg(test)]
mod tests {
    use super::*;
    //use quickcheck_macros::quickcheck;
    #[test]
    fn file_serialization() {
        use std::collections::BTreeMap;
        let mut tasks = BTreeMap::new();
        tasks.insert(
            "task1".into(),
            Task {
                description: None,
                scripts: Script::new("echo hello world".into()),
                depends_on: vec![],
            },
        );
        tasks.insert(
            "task2".into(),
            Task {
                description: None,
                scripts: Script::new("sleep 10".into()),
                depends_on: vec!["task1".into()],
            },
        );

        let data = Data {
            tasks,
            version: None,
        };
        let s = serde_yaml::to_string(&data).unwrap();

        let same: Data = serde_yaml::from_str(&s).unwrap();
        assert_eq!(data, same);
    }
}
