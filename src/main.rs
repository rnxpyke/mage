use dialoguer::*;
use fehler::throws;
use petgraph::prelude::*;

mod app;
mod format;
use format::*;

// a more generic type would be possible, but I don't know how to make that work on caller side
// Generic type:: <T>(items: &[T]) -> usize
fn choose_first(_items: &[&str]) -> usize {
    0
}

// a more generic type would be possible, but I don't know how to make that work on caller side
// Generic type:: <T>(items: &[T]) -> usize
fn choose_interactive(items: &[&str]) -> usize {
    Select::with_theme(&theme::ColorfulTheme::default())
        .with_prompt("select task:")
        .default(0)
        .items(items)
        .interact()
        .unwrap()
}

fn run_tasks<F>(d: &Data, g: &mut DiGraph<String, ()>, choose: F)
where
    F: Fn(&[&str]) -> usize,
{
    use petgraph::algo::toposort;

    // TODO: remove first case
    if false {
        let topo = toposort(&*g, None).unwrap();
        for &node in topo.iter().rev() {
            let name = g.node_weight(node).unwrap();
            let task = d.tasks.get(name).unwrap();
            run_task(task, &name);
        }
    } else {
        let mut externals: Vec<_> = g.externals(Direction::Outgoing).collect();
        let mut nums = externals.len();
        while nums != 0 {
            let selection = if nums == 1 {
                0
            } else {
                choose(
                    &externals
                        .iter()
                        .map(|&node| g.node_weight(node).unwrap().as_ref())
                        .collect::<Vec<&str>>()[..],
                )
            };
            let node = g.node_weight(externals[selection]).unwrap();
            run_task(&d.tasks.get(node).unwrap(), node);
            g.remove_node(externals[selection]);
            externals = g.externals(Direction::Outgoing).collect();
            nums = externals.len();
        }
    }
}

fn run_task(task: &Task, name: &str) {
    use subprocess::ExitStatus;
    let mut exit = ExitStatus::Undetermined;
    let mut default = 0;

    while !exit.success() {
        let selection = Select::with_theme(&theme::ColorfulTheme::default())
            .with_prompt(task.description.as_deref().unwrap_or(name))
            .default(default)
            .items(&task.scripts[..])
            .interact()
            .unwrap();
        use subprocess::Exec;
        exit = Exec::shell(&task.scripts[selection]).join().unwrap();
        default = (default + 1) % task.scripts.len();
    }
}

#[throws(anyhow::Error)]
fn main() {
    app::app()?
}
