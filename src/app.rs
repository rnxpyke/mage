use super::*;
use anyhow::*;
use clap::Clap;
use fehler::throws;
use regex::Regex;
use std::ffi::OsStr;
use std::fs::File;
use std::path::{Path, PathBuf};

trait Runnable {
    fn run(&mut self) -> anyhow::Result<()>;
}

mod runners {
    use crate::app::*;
    use crate::format::*;

    impl Runnable for CheckFile {
        #[throws(anyhow::Error)]
        fn run(&mut self) {
            let reader = magefile_open(&self.file)?;
            let deserialized = Data::from_reader(reader)?;
            if self.data_show {
                println!("{:?}", deserialized);
            }
            let deps = build_graph_unchecked(&deserialized)?;
            use petgraph::dot::{Config, Dot};
            if self.graph_show {
                println!("{:?}", Dot::with_config(&deps, &[Config::EdgeNoLabel]));
            }
            ensure!(
                !petgraph::algo::is_cyclic_directed(&deps),
                "There can't be cyclic dependencies"
            );
        }
    }

    impl Runnable for ParseYaml {
        #[throws(anyhow::Error)]
        fn run(&mut self) {
            let reader = magefile_open(&self.file)?;
            let deserialized: serde_yaml::Value = serde_yaml::from_reader(reader)?;
            println!("{:?}", deserialized);
        }
    }

    impl Runnable for Run {
        #[throws(anyhow::Error)]
        fn run(&mut self) {
            let reader = magefile_open(&self.file)?;
            let deserialized = Data::from_reader(reader)?;
            let mut deps = build_graph(&deserialized)?;
            let choose = if self.auto_choose {
                choose_first
            } else {
                choose_interactive
            };
            run_tasks(&deserialized, &mut deps, choose);
        }
    }
    impl Runnable for Wand {
        #[throws(anyhow::Error)]
        fn run(&mut self) {
            use Spells::*;
            match &self.spells {
                Prompt {
                    prompt,
                    default,
                    initial_text,
                    matches,
                } => {
                    let theme = theme::ColorfulTheme::default();
                    let mut builder = Input::with_theme(&theme);
                    if !prompt.is_empty() {
                        builder.with_prompt(prompt.join(" "));
                    }
                    if let Some(default) = default {
                        builder.default(default.clone());
                    }
                    if let Some(initial) = initial_text {
                        builder.with_initial_text(initial);
                    }
                    if let Some(ex) = matches.clone() {
                        builder.validate_with(move |s: &str| -> Result<(), &str> {
                            if ex.is_match(s) {
                                Ok(())
                            } else {
                                Err("Doesn't match regex")
                            }
                        });
                    }
                    let res: String = builder.interact().unwrap();
                    println!("{}", res);
                }
                Confirm {
                    prompt,
                    false_default,
                } => {
                    let theme = theme::SimpleTheme;
                    let mut builder = dialoguer::Confirm::with_theme(&theme);
                    builder.default(*false_default);
                    if !prompt.is_empty() {
                        builder.with_prompt(prompt.join(" "));
                    }
                    let status = builder.interact().unwrap();
                    let code = if status { 0 } else { 1 };
                    std::process::exit(code);
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
struct Magefile(PathBuf);

#[throws(anyhow::Error)]
fn magefile_open(magefile: &Option<Magefile>) -> File {
    let fd = magefile.clone().unwrap_or_default();
    File::open(&fd).with_context(|| match &magefile {
        None => format!("Can't find file: {:?}\nTry creating a default Magefile or specify the file with the -f option", fd.0),
        Some(path) => format!("Can' find file: {:?}", path.0),
    })?
}

impl<T: ?Sized + AsRef<OsStr>> From<&T> for Magefile {
    fn from(s: &T) -> Magefile {
        Magefile(s.into())
    }
}

impl From<Magefile> for PathBuf {
    #[inline]
    fn from(m: Magefile) -> PathBuf {
        m.0
    }
}

impl AsRef<Path> for Magefile {
    fn as_ref(&self) -> &Path {
        self.0.as_ref()
    }
}

impl Default for Magefile {
    fn default() -> Self {
        Magefile(PathBuf::from("Magefile.yml"))
    }
}

#[derive(Debug, Clap)]
struct CheckFile {
    #[clap(short, parse(from_os_str))]
    file: Option<Magefile>,
    /// Show the internal data representation
    #[clap(short)]
    data_show: bool,
    /// Show the dependency graph
    #[clap(short)]
    graph_show: bool,
}

#[derive(Debug, Clap)]
struct ParseYaml {
    #[clap(parse(from_os_str))]
    file: Option<Magefile>,
}

#[derive(Debug, Clap)]
struct Run {
    /// Which Magefile to use.
    /// Default: Magefile.yml
    #[clap(short, parse(from_os_str))]
    file: Option<Magefile>,
    /// changes the next spell automatically if set
    #[clap(short, long)]
    auto_choose: bool,
}

#[derive(Debug, Clap)]
enum Spells {
    /// Prompt the user for input
    Prompt {
        /// The prompt that is displayed to the user
        prompt: Vec<String>,
        #[clap(short, long)]
        /// The default value to use when the prompt is empty
        default: Option<String>,
        /// The prefilled text
        #[clap(short, long, name("text"))]
        initial_text: Option<String>,
        /// Validate input with a regex
        #[clap(short, long)]
        matches: Option<Regex>,
    },

    Confirm {
        prompt: Vec<String>,
        #[clap(short, parse(from_flag = std::ops::Not::not))]
        false_default: bool,
    },
}

#[derive(Debug, Clap)]
struct Wand {
    #[clap(subcommand)]
    spells: Spells,
}

/// a CLI wizard tool similar to make but intended for interactive usage.
#[derive(Debug, Clap)]
enum SubCommands {
    /// Check for errors in your spellbook
    #[clap(name = "study")]
    CheckFile(CheckFile),
    /// Debug: Read the spellbook and check for syntactical errors
    #[clap(name = "read")]
    ParseYaml(ParseYaml),
    /// Cast Spells from a spellbook
    #[clap(name = "cast")]
    Run(Run),
    /// A mages toolbox
    Wand(Wand),
}

#[clap(name = "mage")]
#[derive(Debug, Clap)]
struct Opts {
    #[clap(subcommand)]
    subcmd: SubCommands,
}

#[throws(anyhow::Error)]
pub fn app() {
    let opts: Opts = Opts::parse();

    use SubCommands::*;
    let mut cmd: Box<dyn Runnable> = match opts.subcmd {
        CheckFile(cmd) => Box::new(cmd),
        ParseYaml(cmd) => Box::new(cmd),
        Run(cmd) => Box::new(cmd),
        Wand(cmd) => Box::new(cmd),
    };
    cmd.run()?
}
#[cfg(test)]
mod tests {
    use super::*;
    fn run_check_file(f: PathBuf) {
        let mut cmd = CheckFile {
            file: Some(Magefile(f)),
            graph_show: false,
            data_show: false,
        };
        cmd.run().unwrap();
    }

    macro_rules! check {
        ($func:ident, $file:tt) => {
            #[test]
            fn $func() {
                run_check_file($file.into())
            }
        };
        (panic, $func:ident, $file:tt) => {
            #[test]
            #[should_panic]
            fn $func() {
                run_check_file($file.into())
            }
        };
    }

    macro_rules! check_ok {
        ($name:ident) => {
            check!($name, {
                concat!("examples/ok/", stringify!($name), ".yml")
            });
        };
    }
    macro_rules! check_fail {
        ($name:ident) => {
            #[should_panic]
            check!(panic, $name, {
                concat!("examples/err/", stringify!($name), ".yml")
            });
        };
    }

    check!(magefile, "Magefile.yml");
    check_ok!(depend_array);
    check_ok!(dependency);
    check_ok!(fails_twice);

    check_ok!(script_single);
    check_ok!(script_multiple);

    check_fail!(cyclic);
}
